const express = require('express')
const app = express()
const PORTA = process.env.PORT || 8080
const info = require('./info.json')
const estudante = require('./estudante.json')

app.listen(PORTA, function(){
    console.log(`Servidor iniciado na porta ${PORTA}`)
})

app.get('/api/roupas', function(req, res){
    const roupas = {
        "tipo": info.tipo[Math.floor(Math.random() * info.tipo.length)],
        "tamanho": info.linha[Math.floor(Math.random() * info.linha.length)],
        "marca": info.marcas[Math.floor(Math.random() * info.marcas.length)],
        "quantidade": Math.floor(Math.random() * (100 - 0) + 0),
        "disponivel": Math.random() > 0.5,
    }
    res.json(roupas)
})

app.use('/api/info', function(req, res){
    const aluno = {
        "nome": estudante.nome,
        "disciplina": estudante.disciplina
    }
    res.json(aluno)
})